<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="welcome" method="post">
      @csrf
      <label for="fname">First name:</label><br /><br />
      <input type="text" name="fname" id="fname" /><br /><br />
      <label for="lname">Last name:</label><br /><br />
      <input type="text" name="lname" id="lname" /><br /><br />
      <input type="submit" value="welcome" />
      <label for="">Gender:</label><br /><br />
      <input type="radio" name="male" id="male" />Male<br />
      <input type="radio" name="female" id="female" />Female<br />
      <input type="radio" name="other" id="other" />Other<br /><br />
      <!-- Negara -->
      <label for="nationality">Nationality:</label><br /><br />
      <select name="nationality" id="">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="singapura">Singapura</option>
        <option value="brunei">Brunei</option></select
      ><br /><br />
      <label for="language">Language Spoken:</label><br /><br />
      <input type="checkbox" name="bindo" id="bindo" />Bahasa Indonesia <br />
      <input type="checkbox" name="bingg" id="bingg" />English <br />
      <input type="checkbox" name="other" id="other" />Other <br /><br />
      <label for="bio">Bio</label><br /><br />
      <textarea name="" id="" cols="30" rows="10"></textarea>
      <br />
      <input type="submit" value="welcome">
    </form>
  </body>
</html>
