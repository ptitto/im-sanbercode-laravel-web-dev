<?php

use Illuminate\Support\Facades\Route;
// use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [HomeController::class, 'home']);
Route::get('/master', [HomeController::class, 'index']);
Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'welcome']);


Route::get('/table', [HomeController::class, 'table']);
Route::get('/data-table', [HomeController::class, 'dataTable']);

// CAST
// Create Cast
// Form Cast
Route::get('/cast/create', [CastController::class, 'create']);
// from post data to database
Route::post('/cast', [CastController::class, 'store']);
// Form Detail cast
Route::get('/cast', [CastController::class, 'index']);
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{cast_id}/', [CastController::class, 'update']);

Route::delete('/cast/{cast_id}/', [CastController::class, 'destroy']);

Route::resolved('film', FilmController::class);