@extends('layout.master')

@section('judul')
    <h1>Halaman CAST</h1>
@endsection
@section('content')
<form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label for="formGroupExampleInput">Nama</label>
      <input type="text" class="form-control" id="nama" name="nama" placeholder="Your Nama">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label for="formGroupExampleInput2">Umur</label>
      <input type="text" class="form-control" id="umur" name="umur" placeholder="Your Age">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label for="formGroupExampleInput3">Bio</label>
        <input type="text" class="form-control" id="bio" name="bio" placeholder="Your Bio">
      </div>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
      <br />
      <input type="submit" value="store">
  </form>
@endsection