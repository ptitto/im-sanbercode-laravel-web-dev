<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function home()
    {
        return view('layout.master');
    }

    public function table()
    {
        return view('partial.table');
    }

    public function dataTable()
    {
        return view('partial.data-table');
    }

    public function index()
    {
        return view('partial.index');
    }
}