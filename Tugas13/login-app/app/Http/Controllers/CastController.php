<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use League\CommonMark\Extension\Table\Table;

class CastController extends Controller
{
    //
    public function create()
    {
        return view('partial.cast');
    }

    public function store(Request $request){

        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        
        return redirect('/cast');
    }


    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('partial.tampil', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('partial.detail', ['cast' => $cast]);

    }

    public function edit($id)
    {
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('partial.edit', ['cast' => $cast]);

    }

    public function update(Request $request, $id){
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')
            ->where('id', $id)
            ->update(
                ['nama' => $request->nama,
                'umur' => $request->umur,
                'bio' => $request->bio]
            );
        return redirect('/cast');
    }

    
    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast');
    }
}