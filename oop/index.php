<?php
require("animal.php");
require("frog.php");
require("ape.php");
$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br/>";
echo "legs : " . $sheep->legs . "<br/>";
echo "cold blooded : " . $sheep->cold_blooded;

echo "<br/>";
echo "<br/>";

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br/>";
echo "legs : " . $kodok->legs . "<br/>";
echo "cold blooded : " . $kodok->cold_blooded . "<br/>";
echo $kodok->jump();

echo "<br/>";
echo "<br/>";

$sungkong = new Ape("kera sakti");
echo "Name : " . $sungkong->name . "<br/>";
echo "legs : " . $sungkong->legs . "<br/>";
echo "cold blooded : " . $sungkong->cold_blooded . "<br/>";
echo $sungkong->yell();
?>
